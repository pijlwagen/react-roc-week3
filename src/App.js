import './App.scss';
import Card from './components/Card';
import Button from './components/Button'

function App() {
  return (
    <div id="app">
      <div className='container'>
        <Card 
          title="Card title" 
          text="Card text" 
          image="//via.placeholder.com/2560x600"
          actions={
            <>
              <Button text="Go somewhere"/>
              <Button text="Go somewhere else" color="secondary" className=""/>
            </>
          }
        />
      </div>
    </div>
  );
}

export default App;
