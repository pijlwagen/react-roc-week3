function Card({ 
    title = null, 
    text, 
    image = null, 
    imageAlt = null, 
    actions = null
}) {
    return (
        <div className='card'>
            {image && <img className='card-img-top' src={image} alt={imageAlt || 'no alt given'} />}
            <div className='card-body'>
                {title && <div className='card-title'><h5>{ title }</h5></div>}
                <div className="card-text">{ text }</div>
                {actions}
            </div>
        </div>
    )
}

export default Card;