Dag zonnebloemetjes,

Het huiswerk is om een Navbar component, gebruik de card als inspiratie. De relatieboom ziet er als volgend uit:
- Navbar
    - Logo
    - NavItem

De navbar is dus een aparte component, waarbij er een attribuut (property) word megegeven met alle items die in de navbar moeten komen.

In je App.js ziet dat er dus als volgend uit:

```jsx
<Navbar 
    logo="//via.placeholder.com/20x20"
    items={[
        {text: 'Home', href="/"},
        {text: 'Contact', href="/contact"},
        {text: 'Pricing', href="#", disabled: true}
    ]}
/>
```

En dit word dan een [bootstrap navbar](https://getbootstrap.com/docs/5.3/components/navbar/#nav) met de 3 items waarvan de laatste disabled is. Success!